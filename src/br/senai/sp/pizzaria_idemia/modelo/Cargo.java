package br.senai.sp.pizzaria_idemia.modelo;

public enum Cargo {
	ATENDENTE, 
	GERENTE, 
	MOTOBOY, 
	PIZZAIOLO
}

