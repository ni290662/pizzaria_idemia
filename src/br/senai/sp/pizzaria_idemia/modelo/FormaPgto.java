package br.senai.sp.pizzaria_idemia.modelo;

public enum FormaPgto {
	
	DINHEIRO,
	CARTAO_DEB,
	CARTAO_CRED,
	VALE_REFEICAO

}
