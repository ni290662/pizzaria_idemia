package br.senai.sp.pizzaria_idemia.modelo;

public enum TipoProduto {
	BEBIDA, 
	PIZZA_DOCE, 
	PIZZA_SALGADA, 
	BROTO_DOCE, 
	BROTO_SALGADA, 
	BORDA_RECHEADA

}
